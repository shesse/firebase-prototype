import { AngularFirestore } from '@angular/fire/firestore'
import { ItemService } from './../service/item.service'
import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  constructor (
    public itemService: ItemService,
    private firestore: AngularFirestore
  ) {}

  ngOnInit (): void {
    this.resetForm()
  }

  resetForm (form?: NgForm) {
    if (form != null) form.resetForm()
    this.itemService.formData = {
      id: null,
      name: '',
      color: ''
    }
  }

  onSubmit (form: NgForm) {
    let data = Object.assign({}, form.value)
    delete data.id
    if (data.id == null) {
      this.itemService.createItem(data)
    } else {
      this.itemService.updateItem(data)
    }
    this.resetForm(form)
  }
}
