import { Item } from './../service/item.model'
import { ItemService } from './../service/item.service'
import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
  list: Item[]
  constructor (private itemService: ItemService) {}

  ngOnInit (): void {
    this.itemService.getItems().subscribe(data => {
      this.list = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...(e.payload.doc.data() as object)
        } as Item;
      })
    });
  }

  onEdit(item: Item) {
    this.itemService.formData = Object.assign({}, item);
  }

  onDelete(id: string) {
    this.itemService.deleteItem(id);
  }
}
