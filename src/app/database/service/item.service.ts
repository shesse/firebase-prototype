import { AngularFirestore } from '@angular/fire/firestore'
import { Item } from './item.model'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  formData: Item

  constructor (private firestore: AngularFirestore) {}

  getItems () {
    return this.firestore.collection('item').snapshotChanges()
  }

  createItem (item: Item) {
    return this.firestore.collection('item').add(item)
  }

  updateItem (item: Item) {
    delete item.id
    this.firestore.doc('item/' + item.id).update(item)
  }

  deleteItem (itemId: string) {
    this.firestore.doc('item/' + itemId).delete()
  }
}
