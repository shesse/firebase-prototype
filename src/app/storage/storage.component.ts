import { Component } from '@angular/core'
import { AngularFireStorage } from '@angular/fire/storage'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent {
  downloadURL: Observable<string>

  constructor (private afStorage: AngularFireStorage) {}

  upload (file) {
    this.afStorage.ref('Bild').put(file.target.files[0])
  }

  download () {
    this.downloadURL = this.afStorage.ref('Bild').getDownloadURL()
  }
}
