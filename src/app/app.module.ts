import { ItemService } from './database/service/item.service';
import { NgModule } from '@angular/core'
import { MaterialModule } from './material/material.module'
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module'

// Firebase services + enviorment module
import { environment } from '../environments/environment'
import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { AngularFireStorageModule } from '@angular/fire/storage'
import { AngularFireAuthGuard } from '@angular/fire/auth-guard'

// Components
import { AppComponent } from './app.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { SignInComponent } from './auth/sign-in/sign-in.component'
import { SignUpComponent } from './auth/sign-up/sign-up.component'
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component'
import { StorageComponent } from './storage/storage.component';
import { DatabaseComponent } from './database/database.component';
import { ItemComponent } from './database/item/item.component';
import { ItemListComponent } from './database/item-list/item-list.component'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    StorageComponent,
    DatabaseComponent,
    ItemComponent,
    ItemListComponent
  ],
  imports: [
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MaterialModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule
  ],
  providers: [AngularFireAuthGuard, ItemService],
  bootstrap: [AppComponent]
})
export class AppModule {}
