import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

// Components
import { SignInComponent } from './auth/sign-in/sign-in.component'
import { SignUpComponent } from './auth/sign-up/sign-up.component'
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component'
import { DashboardComponent } from './dashboard/dashboard.component'

// Angular Firebase Guard
import {
  AngularFireAuthGuard,
  redirectUnauthorizedTo,
  redirectLoggedInTo
} from '@angular/fire/auth-guard'

const redirectUnauthorizedToSignIn = () => redirectUnauthorizedTo(['sign-in'])
const redirectLoggedInToDashboard = () => redirectLoggedInTo(['dashboard'])

// Include route guard in routes array
const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  {
    path: 'sign-in',
    component: SignInComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectLoggedInToDashboard }
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectLoggedInToDashboard }
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectLoggedInToDashboard }
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToSignIn }
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
